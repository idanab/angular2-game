/**
 * Created by owner on 11/01/2017.
 */
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {GameModule} from "./game/game.module";
//import { MaterialModule } from '@angular/material';

@NgModule({
    declarations:[
        AppComponent,
    ],
    providers:[],
    bootstrap   :[AppComponent],
    imports     :[
        BrowserModule,
        GameModule,
        //MaterialModule.forRoot(),
    ],
    exports     :[]
})
export class AppModule{}