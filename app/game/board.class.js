"use strict";
var game_interfaces_1 = require("./game.interfaces");
/**
 * Created by Yevgniy on 08/01/2017.
 */
var Board = (function () {
    function Board(level, x) {
        this.level = level;
        if (typeof x) {
            this.initCellsWithSize(x);
        }
        else {
            this.initCellsWithSize(10);
        }
    }
    Board.prototype.up = function () {
        if (this.currentLocation.x > 0 &&
            this.cells[this.currentLocation.x - 1][this.currentLocation.y] != game_interfaces_1.Cell.visited) {
            this.currentLocation.x -= 1;
            return this.currentLocation;
        }
        return this.currentLocation;
    };
    Board.prototype.down = function () {
        if (this.currentLocation.x < 9 &&
            this.cells[this.currentLocation.x + 1][this.currentLocation.y] != game_interfaces_1.Cell.visited) {
            this.currentLocation.x += 1;
            return this.currentLocation;
        }
        return this.currentLocation;
    };
    Board.prototype.left = function () {
        if (this.currentLocation.y > 0 &&
            this.cells[this.currentLocation.x][this.currentLocation.y - 1] != game_interfaces_1.Cell.visited) {
            this.currentLocation.y -= 1;
            return this.currentLocation;
        }
        return this.currentLocation;
    };
    Board.prototype.right = function () {
        if (this.currentLocation.y < 9 &&
            this.cells[this.currentLocation.x][this.currentLocation.y + 1] != game_interfaces_1.Cell.visited) {
            this.currentLocation.y += 1;
            return this.currentLocation;
        }
        return this.currentLocation;
    };
    Board.prototype.initCellsWithSize = function (x) {
        var arrayOfArrays = [];
        for (var i = 0; i < x; i++) {
            arrayOfArrays.push(new Array(x));
        }
        this.cells = arrayOfArrays;
        for (var i = 0; i < x; i++) {
            for (var j = 0; j < x; j++) {
                this.cells[i][j] = game_interfaces_1.Cell.empty;
            }
        }
        this.currentLocation = { x: Math.floor(x / 2), y: Math.floor(x / 2) };
        this.cells[Math.floor(x / 2)][Math.floor(x / 2)] = game_interfaces_1.Cell.visited;
        console.log('Start Location is:' + [this.currentLocation.x, this.currentLocation.y]);
        this.fillBombLevel(this.level);
    };
    Board.prototype.fillBombLevel = function (level) {
        var randX, randY;
        var lvl = this.level;
        while (lvl > 0) {
            randX = Math.floor(Math.random() * 10);
            randY = Math.floor(Math.random() * 10);
            if (this.cells[randX][randY] != game_interfaces_1.Cell.boom && this.cells[randX][randY] != game_interfaces_1.Cell.visited) {
                this.cells[randX][randY] = game_interfaces_1.Cell.boom;
                console.log(lvl + ' Bomb Location:' + [randX, randY]);
                lvl -= 1;
            }
        }
    };
    return Board;
}());
exports.Board = Board;
//# sourceMappingURL=board.class.js.map