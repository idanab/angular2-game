import {IBoard, Cell, Location} from "./game.interfaces";
/**
 * Created by Yevgniy on 08/01/2017.
 */
export class Board implements IBoard {
    cells: Cell[][];
    currentLocation: Location;
    private level: number;
    constructor(level: number, x?: number) {
        this.level = level;
        if (typeof x) {
            this.initCellsWithSize(x);
        }
        else {
            this.initCellsWithSize(10);
        }
    }

    up(): Location {
        if (this.currentLocation.x > 0 &&
            this.cells[this.currentLocation.x-1][this.currentLocation.y] != Cell.visited) {
            this.currentLocation.x -= 1;
            return this.currentLocation;
        }
        return this.currentLocation;
    }

    down(): Location {
        if (this.currentLocation.x < 9 &&
            this.cells[this.currentLocation.x+1][this.currentLocation.y] != Cell.visited) {
            this.currentLocation.x += 1;
            return this.currentLocation;
        }
        return this.currentLocation;
    }

    left(): Location {
        if (this.currentLocation.y > 0 &&
            this.cells[this.currentLocation.x][this.currentLocation.y-1] != Cell.visited) {
            this.currentLocation.y -= 1;
            return this.currentLocation;
        }
        return this.currentLocation;
    }

    right(): Location {
        if (this.currentLocation.y < 9 &&
            this.cells[this.currentLocation.x][this.currentLocation.y+1] != Cell.visited) {
            this.currentLocation.y += 1;
            return this.currentLocation;
        }
        return this.currentLocation;
    }

    initCellsWithSize(x: number) {
        let arrayOfArrays = [];
        for(let i=0;i<x;i++){
            arrayOfArrays.push(new Array(x));
        }
        this.cells = arrayOfArrays;
        for (let i = 0; i < x; i++) {
            for (let j = 0; j < x; j++) {
                this.cells[i][j] = Cell.empty;
            }
        }
        this.currentLocation = {x: Math.floor(x/2), y: Math.floor(x/2)} as Location;
        this.cells[Math.floor(x/2)][Math.floor(x/2)] = Cell.visited;
        console.log('Start Location is:' + [this.currentLocation.x,this.currentLocation.y]);
        this.fillBombLevel(this.level)
    }

    private fillBombLevel(level:number){
        let randX: number,randY: number;
        let lvl = this.level;
        while(lvl>0){
            randX = Math.floor(Math.random()*10);
            randY = Math.floor(Math.random()*10);
            if(this.cells[randX][randY] != Cell.boom && this.cells[randX][randY] != Cell.visited) {
                this.cells[randX][randY] = Cell.boom;
                console.log(lvl +' Bomb Location:' + [randX,randY] as string);
                lvl -= 1;
            }
        }
    }

}