/**
 * Created by owner on 15/01/2017.
 */
import {Component, Input} from "@angular/core";
import {Cell} from "./game.interfaces";

@Component({
  selector: 'game-board',
  styles : [],
  template: `
<div>
  <table>
        <tr>
            <td colspan="3">
                <table>
                    <tr *ngFor="let row of cells">
                        <td  *ngFor="let col of row"
                             [innerHTML]="col|myPipe"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
`})
export class BoardComponent {
    @Input() cells:Cell[][];

}