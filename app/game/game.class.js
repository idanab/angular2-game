"use strict";
var game_interfaces_1 = require("./game.interfaces");
var board_class_1 = require("./board.class");
/**
 * Created by Yevgniy on 09/01/2017.
 */
var Game = (function () {
    function Game(level, boardSize) {
        this.score = 0;
        this.level = level;
        this.boardSize = boardSize;
        this.board = new board_class_1.Board(level, boardSize);
        this.statusMessage = "start playing...";
    }
    Game.prototype.up = function () {
        var prevLoc = { x: this.board.currentLocation.x, y: this.board.currentLocation.y };
        var loc = this.board.up();
        this.safeOrNot(loc, prevLoc);
        return loc;
    };
    Game.prototype.down = function () {
        var prevLoc = { x: this.board.currentLocation.x, y: this.board.currentLocation.y };
        var loc = this.board.down();
        this.safeOrNot(loc, prevLoc);
        return loc;
    };
    Game.prototype.left = function () {
        var prevLoc = { x: this.board.currentLocation.x, y: this.board.currentLocation.y };
        var loc = this.board.left();
        this.safeOrNot(loc, prevLoc);
        return loc;
    };
    Game.prototype.right = function () {
        var prevLoc = { x: this.board.currentLocation.x, y: this.board.currentLocation.y };
        var loc = this.board.right();
        this.safeOrNot(loc, prevLoc);
        return loc;
    };
    Game.prototype.safeOrNot = function (loc, prevLoc) {
        var currLocX = loc.x, currLocY = loc.y;
        if (this.board.cells[currLocX][currLocY] == game_interfaces_1.Cell.boom) {
            //You Lost
            this.statusMessage = 'YOU LOST\nFinal Score is:' + this.score;
            this.score = 0;
            this.board.initCellsWithSize(10);
            window.location.reload();
            this.statusMessage = "New Game Initialized!";
        }
        else if (loc.x == prevLoc.x && loc.y == prevLoc.y) {
            this.statusMessage = 'Already visited, Try a different route.';
        }
        else {
            this.score += 1;
            this.board.cells[currLocX][currLocY] = game_interfaces_1.Cell.visited;
            console.log('Moved To Location:' + [currLocX, currLocY]);
            this.statusMessage = 'You\'re Safe.\nCurrent score is:' + this.score;
        }
    };
    return Game;
}());
exports.Game = Game;
//# sourceMappingURL=game.class.js.map