import {IGame, IBoard, Location, Cell} from "./game.interfaces";
import {Board} from "./board.class";
/**
 * Created by Yevgniy on 09/01/2017.
 */

export class Game implements IGame{

    score: number = 0;
    level: number;
    board: Board;
    public statusMessage:string;
    private boardSize: number;

    constructor(level: number, boardSize: number) {
        this.level = level;
        this.boardSize = boardSize;
        this.board = new Board(level,boardSize);
        this.statusMessage= "start playing..."
    }

    up(): Location {
        let prevLoc = {x:this.board.currentLocation.x,y:this.board.currentLocation.y} as Location;
        let loc = this.board.up();
        this.safeOrNot(loc,prevLoc);
        return loc;
    }


    down(): Location {
        let prevLoc = {x:this.board.currentLocation.x,y:this.board.currentLocation.y} as Location;
        let loc = this.board.down();
        this.safeOrNot(loc,prevLoc);
        return loc;
    }

    left(): Location {
        let prevLoc = {x:this.board.currentLocation.x,y:this.board.currentLocation.y} as Location;
        let loc = this.board.left();
        this.safeOrNot(loc,prevLoc);
        return loc;
    }

    right(): Location {
        let prevLoc = {x:this.board.currentLocation.x,y:this.board.currentLocation.y} as Location;
        let loc = this.board.right();
        this.safeOrNot(loc,prevLoc);
        return loc;
    }

    private safeOrNot(loc: Location, prevLoc: Location) {
        let currLocX = loc.x,currLocY = loc.y;
        if(this.board.cells[currLocX][currLocY] == Cell.boom){
            //You Lost
            this.statusMessage ='YOU LOST\nFinal Score is:' + this.score as string;
            this.score = 0;
            this.board.initCellsWithSize(10);
            window.location.reload();
            this.statusMessage ="New Game Initialized!";
        }
        else if(loc.x == prevLoc.x && loc.y == prevLoc.y){
            this.statusMessage ='Already visited, Try a different route.';
        }
        else{
            this.score += 1;
            this.board.cells[currLocX][currLocY] = Cell.visited;
            console.log('Moved To Location:'+ [currLocX,currLocY] as string);
            this.statusMessage ='You\'re Safe.\nCurrent score is:' + this.score as string;
        }
    }


}