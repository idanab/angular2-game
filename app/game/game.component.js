"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by owner on 11/01/2017.
 */
var core_1 = require("@angular/core");
var game_service_1 = require("./game.service");
var GameComponent = (function () {
    function GameComponent(gameService) {
        this.gameService = gameService;
    }
    GameComponent.prototype.ngOnInit = function () {
        this.cells = this.gameService.getCells();
        this.comments = this.gameService.getComments();
    };
    GameComponent.prototype.up = function () { this.gameService.up(); };
    GameComponent.prototype.down = function () { this.gameService.down(); };
    GameComponent.prototype.right = function () { this.gameService.right(); };
    GameComponent.prototype.left = function () { this.gameService.left(); };
    GameComponent.prototype.sendComment = function (val) {
        this.gameService.setComment(val);
    };
    return GameComponent;
}());
GameComponent = __decorate([
    core_1.Component({
        selector: 'my-game',
        providers: [game_service_1.GameService],
        moduleId: module.id,
        styles: [],
        templateUrl: 'game.template.html'
    }),
    __metadata("design:paramtypes", [game_service_1.GameService])
], GameComponent);
exports.GameComponent = GameComponent;
var core_2 = require("@angular/core");
var CellPipe = (function () {
    function CellPipe() {
    }
    CellPipe.prototype.transform = function (value) {
        if (value == 0)
            return "<img src=\"https://cdn2.iconfinder.com/data/icons/basics-vol-2/354/blank_confuse_emoticon_explanation_emoji_faces_emotag-256.png\" width=\"20\" height=\"20\">";
        if (value == 1)
            return "<img src=\"https://s-media-cache-ak0.pinimg.com/736x/d5/63/be/d563be90278dbc69c75a381e560a75f4.jpg\" width=\"20\" height=\"20\">";
        if (value == 2)
            return "<img src=\"http://pix.iemoji.com/hang33/0560.png\" width=\"20\" height=\"20\">";
    };
    return CellPipe;
}());
CellPipe = __decorate([
    core_2.Pipe({ name: 'myPipe' }),
    __metadata("design:paramtypes", [])
], CellPipe);
exports.CellPipe = CellPipe;
//# sourceMappingURL=game.component.js.map