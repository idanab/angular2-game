/**
 * Created by owner on 11/01/2017.
 */
import {Component, OnInit} from "@angular/core";
import {Game} from "./game.class";
import {GameService} from "./game.service";

@Component({
    selector: 'my-game',
    providers: [GameService],
    moduleId:module.id,
    styles : [],
    templateUrl:'game.template.html'})
export class GameComponent implements OnInit{
    constructor(private gameService: GameService){
    }
    cells:Cell[][];
    comments:string[];
    ngOnInit(): void {
        this.cells= this.gameService.getCells();
        this.comments= this.gameService.getComments();
    }

    up(){this.gameService.up();}
    down(){this.gameService.down();}
    right(){this.gameService.right();}
    left(){this.gameService.left();}

    sendComment(val) {
        this.gameService.setComment(val);
    }
}

import { Pipe, PipeTransform } from '@angular/core';
import {Cell} from "./game.interfaces";

@Pipe({name: 'myPipe'})
export class CellPipe implements PipeTransform {
  transform(value) {
        if (value==0)
            return `<img src="https://cdn2.iconfinder.com/data/icons/basics-vol-2/354/blank_confuse_emoticon_explanation_emoji_faces_emotag-256.png" width="20" height="20">`;
        if (value==1)
            return `<img src="https://s-media-cache-ak0.pinimg.com/736x/d5/63/be/d563be90278dbc69c75a381e560a75f4.jpg" width="20" height="20">`;
        if (value==2)
            return `<img src="http://pix.iemoji.com/hang33/0560.png" width="20" height="20">`;
  }
}