/**
 * Created by Eyal on 08/01/2017.
 */
export interface Location{
    x:number;
    y:number
}

export interface Move{
    up():Location;
    down():Location;
    left():Location;
    right():Location;
}

export enum Cell{
    empty,
    visited,
    boom
}

export interface IBoard extends Move{
    cells:Cell[][];
    currentLocation:Location;
}

export interface IGame extends Move{
    score:number;
    level:number;
    board:IBoard;
}

