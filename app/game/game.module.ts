/**
 * Created by owner on 12/01/2017.
 */
import {NgModule} from "@angular/core";
import {GameComponent, CellPipe} from "./game.component";
import {CommonModule} from "@angular/common";
import {MaterialModule} from "@angular/material";
import {BoardComponent} from "./board.component";

@NgModule({
    declarations:[
        GameComponent,
        BoardComponent,
        CellPipe,
    ],
    providers   :[],
    bootstrap   :[],
    imports     :[
        CommonModule,
        //MaterialModule,
    ],
    exports     :[
        GameComponent,
    ]
})
export class GameModule{}