"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by owner on 12/01/2017.
 */
var core_1 = require("@angular/core");
var game_class_1 = require("./game.class");
var GameService = (function () {
    function GameService() {
        this.comments = [];
        this.game = new game_class_1.Game(9, 10);
        this.comments.push("This is the first comment!");
    }
    Object.defineProperty(GameService.prototype, "score", {
        get: function () {
            return this.game.score;
        },
        enumerable: true,
        configurable: true
    });
    GameService.prototype.setComment = function (comment) {
        this.comments.push(comment);
    };
    GameService.prototype.getComments = function () {
        return this.comments;
    };
    Object.defineProperty(GameService.prototype, "level", {
        get: function () {
            return this.game.level;
        },
        enumerable: true,
        configurable: true
    });
    GameService.prototype.getCells = function () {
        return this.game.board.cells;
    };
    GameService.prototype.up = function () {
        this.game.up();
    };
    GameService.prototype.down = function () {
        this.game.down();
    };
    GameService.prototype.right = function () {
        this.game.right();
    };
    GameService.prototype.left = function () {
        this.game.left();
    };
    return GameService;
}());
GameService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], GameService);
exports.GameService = GameService;
//# sourceMappingURL=game.service.js.map