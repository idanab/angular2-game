/**
 * Created by owner on 12/01/2017.
 */
import { Injectable } from '@angular/core';
import {Game} from "./game.class";
import {Board} from "./board.class";
import {Cell} from "./game.interfaces";

@Injectable()
export class GameService {
    public game:Game;
    public comments:string[]=[];
    constructor() {
        this.game= new Game(9,10);
        this.comments.push("This is the first comment!");
    }
    get score() {
        return this.game.score;
    }

    setComment(comment:string){
        this.comments.push(comment);
    }
    getComments() {
        return this.comments;
    }

    get level() {
        return this.game.level;
    }

    getCells() {
        return this.game.board.cells;
    }

    up(){
        this.game.up();
    }
    down(){
        this.game.down();
    }
    right(){
        this.game.right();
    }
    left(){
        this.game.left();
    }

}